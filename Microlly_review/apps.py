from django.apps import AppConfig


class MicrollyReviewConfig(AppConfig):
    name = 'Microlly_review'
